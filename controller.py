#!/usr/bin/env python3

# -*- coding: utf-8 -*-

from tkinter import *
from tkinter.filedialog import *
from bubble import *
from time import *


def checkBubbleStr():
    dataStr = "abcdefghijklmnopqrstuvwxyz"
    toSort = "jklmnopqrstuvwxyzabcdefghi"
    output = bubbleSortStr(toSort)
    if (output == dataStr):
        print('\033[92m Bubble sort OK ! Yay \033[0m')
        return 1
    else:
        print('\033[93m Nope \033[0m')
        return 0
    #

def launchTest():
    total = 0
    nbTest = 1

    elapsed_time = time()
    total += checkBubbleStr()
    elapsed_time = time() - elapsed_time


    if total <= nbTest / 2 :
        print('\033[93m ' + str(total) + ' / ' + str(nbTest) + ' succeeded \033[0m')
    elif total > nbTest / 2 :
        print('\033[92m ' + str(total) + ' / ' + str(nbTest) + ' succeeded \033[0m')

    return {'total':total, 'nbTest':nbTest,'elapsed_time':elapsed_time}
    #


'''
Gui structure :
- launcher
- menu
- sortings
  - bubbleswap
    - visualization
'''

class Application(Tk):
    def __init__(self, name, master=None):
        self.run = True
        Tk.__init__(self,master)
        # self.pack(expand="both")
        # self.configure(height='500',width='500')
        # self.fenetre = Tk()
        self.title("Sorting algorithm visualization")
        self.geometry("500x500")#WxH
        self.GenLauncherFrame()
        #

    def stop(self):
        self.run=False
        self.quit()

    def constructMenu(self):
        self.LauncherDelete()
        self.GenMenuFrame()
        self.GenBottomFrame()
        #

    def bubbleSortPage(self):
        self.menu['frame'].destroy()
        if not self.menu['bubblePageButton'] :
            print('Fuck')
        # toDelLen = len()
        print("Type : " + str(type(self.menu['bubblePageButton'])))
        #print("Options : " + str(self.menu['bubblePageButton'].config()))
        if 'bubblePageButton' in self.menu:
            print(self.menu['bubblePageButton'])
            #self.menu['bubblePageButton'].delete(0)

        # self.menu['bubblePageButton']['command']=self.GenMenuFrame
        self.bubbleSortPage = {}
        self.bubbleSortPage['results'] = launchTest()
        display_str = 'It took ' + str(self.bubbleSortPage['results']['elapsed_time']) + 's to effectuate the ' + str(self.bubbleSortPage['results']['nbTest']) + ' tests and it made ' + str(self.bubbleSortPage['results']['total']) + ' points'
        if not self.bubbleSortPage :
            self.bubbleSortPage = {}
            self.bubbleSortPage['displayLabel'] = Label(self.menu['frame'], text=display_str,)
            self.bubbleSortPage['displayLabel'].pack()
        else:
            if not self.bubbleSortPage:
                self.bubbleSortPage = {}
                self.bubbleSortPage['displayLabel'] = Label(self.menu['frame'], text=display_str,)
                self.bubbleSortPage['displayLabel'].pack()
            self.bubbleSortPage['displayLabel']['text']=display_str
        #

    def GenMenuFrame(self):
        self.menu={}
        self.menu['frame'] = Frame(self, bg='#00ffff',height='600',width='1000')
        self.menu['frame'].grid_propagate(0)
        self.menu['frame'].pack()
        self.menu['bubblePageButton'] = Button(self.menu['frame'], text='Bubble sort', command=self.bubbleSortPage)
        self.menu['bubblePageButton'].pack()
        #

    def LauncherDelete(self):
        self.launcher['skip'].destroy()
        self.launcher['frame'].destroy()
        #

    def GenLauncherFrame(self):
        self.launcher={}
        self.launcher['frame'] = Frame(self, background='#42f47d', height='500', width='500')
        # self.launcher['frame'].grid_propagate(0)
        self.launcher['frame'].pack(fill="both", expand=True)
        # self.launcher['frame'].after(300)
        # print("Start : %s" % ctime())
        # sleep( 5 )
        # print("End : %s" % ctime())
        self.launcher['skip'] = Button(self.launcher['frame'], text='Welcome', command=self.constructMenu)
        self.launcher['skip'].pack(anchor="center")
        #

    def GenBottomFrame(self):
        self.bottom={}
        self.bottom['frame'] = Frame(self,background='#ff00ff', height='200',width='1000')
        self.bottom['frame'].grid_propagate(0)
        self.bottom['frame'].pack()
        self.bottom['quitButton'] = Button(self.bottom['frame'], text='Quit', command=self.stop)
        self.bottom['quitButton'].pack(anchor='center')
        #

    def MainLoop(self):
        while self.run == True:
            self.update_idletasks()
            self.update()
        # self.fenetre.mainloop()
        #

app = Application('Vizualize sort')
app.MainLoop()

#
