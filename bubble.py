#!/usr/bin/env python3

def checkStr(chaine):
    idx = 0
    while idx < len(chaine)-1:
        if chaine[idx] > chaine[idx+1]:
            return False
        idx+=1
    return True

def bubbleSortStr(inputStr):
    listed = list(inputStr)
    isOk = False
    while isOk != True:
        idx = 0
        while idx <= len(listed):
            if idx + 1 < len(listed) and listed[idx] > listed[idx + 1] :
                tmp = listed[idx]
                listed[idx] = listed[idx+1]
                listed[idx+1] =  tmp
            idx+=1
        if checkStr(listed) == True:
            isOk = True
    return ''.join(listed)




#
